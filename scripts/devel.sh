#!/usr/bin/env bash

cd site
stack install \
  --file-watch \
  --exec="../scripts/restart.sh"

