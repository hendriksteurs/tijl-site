{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Tijl.Site where

import Tijl.Site.Constants
import Tijl.Site.Widget
import Yesod

data App = App

mkYesod
  "App"
  [parseRoutes|

/ HomeR GET

/nieuwe-pagina NieuwePaginaR GET

|]

instance Yesod App

getHomeR :: Handler Html
getHomeR = defaultLayout $(widgetFile "home")

getNieuwePaginaR :: Handler Html
getNieuwePaginaR = defaultLayout $(widgetFile "nieuwe-pagina")

tijlSite :: IO ()
tijlSite = warp 8000 App
